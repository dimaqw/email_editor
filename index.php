<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<style>
    input {
        margin-bottom: 5px;
    }
</style>

<script src='vendor/tinymce/tinymce/tinymce.min.js'></script>
<script>
  tinymce.init({
    selector: '#tinymce_editor',
    entity_encoding : "raw",
    height: 400,
    theme: 'modern',
    plugins: [
      'advlist autolink lists link image charmap print preview hr anchor pagebreak',
      'searchreplace wordcount visualblocks visualchars code fullscreen',
      'insertdatetime media nonbreaking save table contextmenu directionality',
      'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
    ],
    toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
    image_advtab: true,
    templates: [
      {title: 'Test template 1', content: 'Test 1'},
      {title: 'Test template 2', content: 'Test 2'}
    ],
    content_css: [
      '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
      '//www.tinymce.com/css/codepen.min.css'
    ],

    // enable title field in the Image dialog
    image_title: true,
    // enable automatic uploads of images represented by blob or data URIs
    automatic_uploads: true,
    // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
    images_upload_url: 'postAcceptor.php',
    // here we add custom filepicker only to Image dialog
    file_picker_types: 'image',
    // and here's our custom image picker
    file_picker_callback: function (cb, value, meta) {
      var input = document.createElement('input');
      input.setAttribute('type', 'file');
      input.setAttribute('accept', 'image/*');



      input.onchange = function () {
        var file = this.files[0];

        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {

          var id = 'blobid' + (new Date()).getTime();
          var blobCache = tinymce.activeEditor.editorUpload.blobCache;
          var base64 = reader.result.split(',')[1];
          var blobInfo = blobCache.create(id, file, base64);
          blobCache.add(blobInfo);

          cb(blobInfo.blobUri(), {title: file.name});
        };
      };

      input.click();
    }
  });

  window.onload = function () {

    document.querySelector('#files').onchange = function () {
      var list = document.getElementById('fileList');
      var input = document.getElementById('files');

      list.innerHTML = '';

      for (var x = 0; x < input.files.length; x++) {
        var li = document.createElement('li');
        li.innerHTML = 'File ' + (x + 1) + ':  ' + input.files[x].name;
        list.append(li);
      }
    };

  }


</script>


<body>
<h1>Написать письмо</h1>
<form id="editor" name="editor" method="post" enctype="multipart/form-data" action="email.php" accept-charset="utf-8">

    <label for="emails">E-mails:</label>
    <input id="emails" type="email" name="emails" multiple required> (Можно указать через запятую)
    <br>

    <label for="theme">Тема</label>
    <input id="theme" type="text" name="theme" size="100">

    <br>
    <label for="tinymce_editor"></label><textarea name="tinymce_editor" id="tinymce_editor">С уважением, Дмитрий.</textarea>
    <br>
    <input id="files" type="file" name="files[]" multiple>
    <ul id="fileList">
        <li>Нет прикрепленных файлов</li>
    </ul>
    <button value="Submit">Отправить</button>

</form>
</body>
