<?php
//ini_set('display_errors', 1);

require 'vendor/phpmailer/phpmailer/PHPMailerAutoload.php';

$mail = new PHPMailer;
//$mail->SMTPDebug = 4;
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'kharl.breger@gmail.com';                 // SMTP username
$mail->Password = '';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

$mail->setFrom('kharl.breger@gmail.com', 'Mailer');

if(empty($_POST['emails'])){
    die('You did not specify any addresses.');
}

$mail->addAddress('d.demydenko@gmail.com', 'Joe User');     // Add a recipient

if(!empty($_FILES['files'])){
    foreach ($_FILES['files']['error'] as $key => $error)
    {
        $tmp_name = $_FILES['files']['tmp_name'][$key];
        if (!$tmp_name) continue;

        $name = basename($_FILES['files']['name'][$key]);

        if ($error == UPLOAD_ERR_OK){
            move_uploaded_file($tmp_name, '/tmp/'.$name);
            $mail->addAttachment('/tmp/'.$name, $name);
        }
        else error_log("Upload error: $error on file $name " . PHP_EOL);
    }
}


if(empty($_POST['tinymce_editor'])){
    die('Body is empty nothing to send.');
}
$html = $_POST['tinymce_editor'];
$dom = new domDocument();

$searchPage = mb_convert_encoding($html, 'HTML-ENTITIES', "UTF-8");
@$dom->loadHTML($searchPage);

$dom->preserveWhiteSpace = false;
$images = $dom->getElementsByTagName('img');

foreach ($images as $image) {
  $t = $image->getAttribute('src');
  $cid = pathinfo($t, PATHINFO_FILENAME);
  $image->setAttribute('src', 'cid:' . $cid);
  $title = $image->getAttribute('title');
  $mail->AddEmbeddedImage($t, $cid, $title);
}


$html = $dom->saveHTML();
$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = '';
if(!empty($_POST['theme'])){
    $mail->Subject = $_POST['theme'];
}
$mail->Body    = $html;
$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'Message has been sent';
}
